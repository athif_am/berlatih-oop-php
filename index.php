<?php
require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new Animal ("shaun");

echo "Hasil Release 0" . "<br><br>"; 
echo "Nama : " . $sheep->name . "<br>";// "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 2
echo "Berdarah Dingin : "; 
var_dump ($sheep->cold_blooded); // false

echo "<br><br>". "Hasil Release 1" . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Nama : " . $sungokong->name . "<br>";
echo "Jumlah Kaki : " . $sungokong->legs . "<br>";
echo "Berdarah Dingin : "; 
var_dump ($sungokong->cold_blooded);
echo "<br>";
echo $sungokong->yell();
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Nama : " . $kodok->name . "<br>";
echo "Jumlah Kaki : " . $kodok->legs . "<br>";
echo "Berdarah Dingin : "; 
var_dump ($kodok->cold_blooded);
echo "<br>";
echo $kodok->jump();
?>